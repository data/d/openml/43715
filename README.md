# OpenML dataset: Federal-Reserve-Interest-Rates-1954-Present

https://www.openml.org/d/43715

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The Federal Reserve sets interest rates to promote conditions that achieve the mandate set by the Congress  high employment, low and stable inflation, sustainable economic growth, and moderate long-term interest rates. Interest rates set by the Fed directly influence the cost of borrowing money. Lower interest rates encourage more people to obtain a mortgage for a new home or to borrow money for an automobile or for home improvement. Lower rates encourage businesses to borrow funds to invest in expansion such as purchasing new equipment, updating plants, or hiring more workers. Higher interest rates restrain such borrowing by consumers and businesses. 
Content
This dataset includes data on the economic conditions in the United States on a monthly basis since 1954. The federal funds rate is the interest rate at which depository institutions trade federal funds (balances held at Federal Reserve Banks) with each other overnight. The rate that the borrowing institution pays to the lending institution is determined between the two banks; the weighted average rate for all of these types of negotiations is called the effective federal funds rate. The effective federal funds rate is determined by the market but is influenced by the Federal Reserve through open market operations to reach the federal funds rate target. The Federal Open Market Committee (FOMC) meets eight times a year to determine the federal funds target rate; the target rate transitioned to a target range with an upper and lower limit in December 2008. The real gross domestic product is calculated as the seasonally adjusted quarterly rate of change in the gross domestic product based on chained 2009 dollars. The unemployment rate represents the number of unemployed as a seasonally adjusted percentage of the labor force. The inflation rate reflects the monthly change in the Consumer Price Index of products excluding food and energy.
Acknowledgements
The interest rate data was published by the Federal Reserve Bank of St. Louis' economic data portal. The gross domestic product data was provided by the US Bureau of Economic Analysis; the unemployment and consumer price index data was provided by the US Bureau of Labor Statistics.
Inspiration
How does economic growth, unemployment, and inflation impact the Federal Reserve's interest rates decisions? How has the interest rate policy changed over time? Can you predict the Federal Reserve's next decision? Will the target range set in March 2017 be increased, decreased, or remain the same?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43715) of an [OpenML dataset](https://www.openml.org/d/43715). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43715/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43715/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43715/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

